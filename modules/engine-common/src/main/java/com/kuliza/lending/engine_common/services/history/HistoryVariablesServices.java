package com.kuliza.lending.engine_common.services.history;

import java.util.Map;

import org.flowable.engine.HistoryService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.exception.ProcessInstanceDoesNotExistException;
import com.kuliza.lending.common.exception.ProcessInstanceNotCompletedException;
import com.kuliza.lending.common.utils.Constants;

@Service
public class HistoryVariablesServices {

	@Autowired
	private HistoryService historyService;

	private static final Logger logger = LoggerFactory.getLogger(HistoryVariablesServices.class);

	/**
	 * 
	 * This functions provides all the process variables from process instance
	 * which is ended.
	 * 
	 * @param parentProcessInstanceId
	 * @return Map containing process variables
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 * @throws ProcessInstanceDoesNotExistException
	 * @throws ProcessInstanceNotCompletedException
	 * 
	 * @author Arpit Agrawal
	 */

	public Map<String, Object> getAllProcessVariablesFromParentProcessInstance(String parentProcessInstanceId)
			throws NullPointerException, IllegalArgumentException, ProcessInstanceDoesNotExistException,
			ProcessInstanceNotCompletedException {
		logger.info("--> Entering getAllProcessVariablesFromParentProcessInstance()");
		if (parentProcessInstanceId == null) {
			throw new NullPointerException("parentProcessInstanceId is null");
		}
		if (!parentProcessInstanceId.matches(Constants.ENGINE_TABLES_ID_REGEX)) {
			throw new IllegalArgumentException(
					"parentProcessInstanceId should match regex : " + Constants.ENGINE_TABLES_ID_REGEX);
		} else {
			logger.debug("getting process variables from history service");
			HistoricProcessInstance parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(parentProcessInstanceId).includeProcessVariables().singleResult();
			if (parentProcessInstance != null) {
				if (parentProcessInstance.getEndTime() == null) {
					throw new ProcessInstanceNotCompletedException(parentProcessInstanceId);
				} else {
					Map<String, Object> processVariables = parentProcessInstance.getProcessVariables();
					processVariables.put("PROC_INST_ID", parentProcessInstance.getId());
					logger.info("<-Exiting getAllProcessVariablesFromParentProcessInstance()");
					return processVariables;
				}
			} else {
				throw new ProcessInstanceDoesNotExistException(parentProcessInstanceId);
			}

		}
	}

}
