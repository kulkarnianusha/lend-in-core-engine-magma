package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.configurator.serializers.BulkRuleSerializer;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "ce_rule")
@Where(clause = "is_deleted=0")
@JsonSerialize(converter = BulkRuleSerializer.class)
public class Rule extends BaseModel {

	@Column(length = 10000, nullable = false)
	private String inputRange;
	@Column(nullable = false)
	private String outputValue;
	@Column(nullable = false)
	private double outputWeight;
	@Column(columnDefinition = "tinyint(1) default 1", nullable = false)
	private boolean isActive;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "variableId", nullable = false)
	private Variable variable;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "groupId", nullable = false)
	private Group group;

	public Rule() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
		this.isActive = true;
	}

	public Rule(long id, String inputRange, String outputValue, double outputWeight, boolean isActive,
			Variable variable, Group group) {
		super();
		this.setId(id);
		this.inputRange = inputRange;
		this.outputValue = outputValue;
		this.outputWeight = outputWeight;
		this.isActive = isActive;
		this.variable = variable;
		this.group = group;
	}

	public Rule(String inputRange, String outputValue, double outputWeight, Variable variable, boolean isActive,
			Group group) {
		this.inputRange = inputRange;
		this.outputValue = outputValue;
		this.outputWeight = outputWeight;
		this.variable = variable;
		this.group = group;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
		this.isActive = isActive;
	}

	@JsonIgnore
	public String getInputRange() {
		return inputRange;
	}

	public void setInputRange(String inputRange) {
		this.inputRange = inputRange;
	}

	public String getOutputValue() {
		return outputValue;
	}

	public void setOutputValue(String outputValue) {
		this.outputValue = outputValue;
	}

	public double getOutputWeight() {
		return outputWeight;
	}

	public void setOutputWeight(double outputWeight) {
		this.outputWeight = outputWeight;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@JsonIgnore
	public Variable getVariable() {
		return variable;
	}

	public void setVariable(Variable variable) {
		this.variable = variable;
	}

	@JsonIgnore
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
