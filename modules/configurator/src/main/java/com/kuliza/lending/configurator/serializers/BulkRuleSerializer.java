package com.kuliza.lending.configurator.serializers;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.pojo.GetRules;
import com.kuliza.lending.configurator.pojo.ResponseRuleDef;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BulkRuleSerializer extends StdConverter<Set<Rule>, List<GetRules>> {

	@Override
	public List<GetRules> convert(Set<Rule> rules) {
		List<GetRules> allGroupRules = new ArrayList<>();
		Map<String, List<ResponseRuleDef>> oneVariableRuleGroup = new HashMap<>();
		for (Rule rule : rules) {
			ResponseRuleDef responseRuleDef = new ResponseRuleDef();
			GetRules oneVariableRules = new GetRules();
			List<ResponseRuleDef> oneVariableruleList = new ArrayList<>();
			String[] ismultiRule = rule.getInputRange().split("&&");
			if (ismultiRule.length > 1 && !rule.getVariable().getType().equals(Constants.STRING)) {
				String[] part1 = ismultiRule[0].trim().split(" ");
				String[] part2 = ismultiRule[1].trim().split(" ");
				responseRuleDef.setFn1(part1[0]);
				responseRuleDef.setValue1(part1[1]);
				responseRuleDef.setFn2(part2[0]);
				responseRuleDef.setValue2(part2[1]);
			} else {
				String[] part1 = rule.getInputRange().split(" ");
				responseRuleDef.setFn1(part1[0]);
				if (rule.getVariable().getType().equals(Constants.STRING)) {
					responseRuleDef.setValue1(rule.getInputRange().replaceAll("=", "").replaceAll("!", "").trim());
				} else {
					responseRuleDef.setValue1(part1[1]);
				}
				responseRuleDef.setFn2("");
				responseRuleDef.setValue2("");
			}
			responseRuleDef.setOutputValue(rule.getOutputValue());
			responseRuleDef.setRuleId(Long.toString(rule.getId()));
			if (oneVariableRuleGroup.containsKey(rule.getVariable().getName())) {
				oneVariableruleList = oneVariableRuleGroup.get(rule.getVariable().getName());
				oneVariableruleList.add(responseRuleDef);
			} else {
				oneVariableruleList.add(responseRuleDef);
				oneVariableRules.setOutputWeight(Double.toString(rule.getOutputWeight()));
				oneVariableRules.setVariableName(rule.getVariable().getName());
				oneVariableRules.setVariableType(rule.getVariable().getType());
				oneVariableRules.setVariableSource(rule.getVariable().getSource());
				oneVariableRules.setIsActive(Boolean.toString(rule.isActive()));
				allGroupRules.add(oneVariableRules);
			}
			oneVariableRuleGroup.put(rule.getVariable().getName(), oneVariableruleList);
		}
		for (GetRules getRule : allGroupRules) {
			getRule.setRules(oneVariableRuleGroup.get(getRule.getVariableName()));
		}
		return allGroupRules;
	}

}
