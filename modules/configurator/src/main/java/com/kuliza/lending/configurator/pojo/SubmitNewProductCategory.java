package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SubmitNewProductCategory {

	@NotNull(message = "productCategoryName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_PRODUCT_CATEGORY_NAME_MESSAGE)
	@Size(max = Constants.MAX_LENGTH_STRING, message = Constants.PRODUCT_CATEGORY_NAME_TOO_BIG)
	String productCategoryName;

	public SubmitNewProductCategory() {
		this.productCategoryName = "";
	}

	public SubmitNewProductCategory(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}

	public String getProductCategoryName() {
		return productCategoryName;
	}

	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("productCategoryName : " + productCategoryName + ", ");
	// inputData.append("outputType : " + outputType);
	// inputData.append(" }");
	// return inputData.toString();
	// }

}
