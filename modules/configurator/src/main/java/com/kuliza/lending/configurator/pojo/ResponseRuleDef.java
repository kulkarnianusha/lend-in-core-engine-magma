package com.kuliza.lending.configurator.pojo;

public class ResponseRuleDef {

	String ruleId;
	String fn1;
	String value1;
	String fn2;
	String value2;
	String outputValue;

	public ResponseRuleDef() {
		super();
	}

	public ResponseRuleDef(String ruleId, String fn1, String value1, String fn2, String value2, String outputValue) {
		super();
		this.ruleId = ruleId;
		this.fn1 = fn1;
		this.value1 = value1;
		this.fn2 = fn2;
		this.value2 = value2;
		this.outputValue = outputValue;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getFn1() {
		return fn1;
	}

	public void setFn1(String fn1) {
		this.fn1 = fn1;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getFn2() {
		return fn2;
	}

	public void setFn2(String fn2) {
		this.fn2 = fn2;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getOutputValue() {
		return outputValue;
	}

	public void setOutputValue(String outputValue) {
		this.outputValue = outputValue;
	}

}
