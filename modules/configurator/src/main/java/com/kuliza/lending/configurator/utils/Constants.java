package com.kuliza.lending.configurator.utils;

public class Constants {

	private Constants() {
		throw new UnsupportedOperationException();
	}

	/********** ALL REGEX USED **********/
	public static final String EMPTY_REGEX = "^$";
	public static final String PRODUCT_CATEGORY_OUTPUT_TYPE = "^(Boolean)|(Numerical)$";
	public static final String ID_REGEX = "^[1-9]+[0-9]*$";
	public static final String EMPTY_OR_ID_REGEX = "(^$)|(^[1-9]+[0-9]*)$";
	public static final String NAME_REGEX = "^([a-zA-Z_]+[0-9\\.]*)+$";
	public static final String NOT_VARIABLE_NAMES_REGEX = "^((IF)|(AND)|(OR)|(FV)|(NPV)|(PMT)|(PPMT)|(RATE)|(NOMINAL)|(IRR)|(TRUE)|(FALSE)|(IPMT)|(PV)|(MIN)|(MAX)|(NOT)|(LOG))$";
	public static final String EXPRESSION_TOKENIZER_REGEX = "(\\s)+|(?<=[(\\+)(\\-)(\\*)%/>=<,(\\))(\\()])|(?=[(\\+)(\\-)(\\*)%/>=<,(\\))(\\()])";
	public static final String VARIABLE_TYPE_REGEX = "^(Numerical)|(String)|(Boolean)$";
	public static final String VARIABLE_CATEGORY_REGEX = "^(Primitive)|(Derived)$";
	public static final String VALUE_REGEX = "^[0-9]+([\\.][0-9]+)?$";
	public static final String IS_BOOLEAN_REGEX = "^(true)|(false)$";
	public static final String EMPTY_OR_VALUE_REGEX = "(^$)|(^[0-9]+[\\.]?[0-9]*)$";
	public static final String LOGICAL_FUNCTIONS_REGEX = "^(>)|(<)|(<=)|(>=)|(!=)|(==)$";
	public static final String EMPTY_OR_LOGICAL_FUNCTIONS_REGEX = "(^$)|(^(>)|(<)|(<=)|(>=)|(!=)|(==)$)";
	public static final String BOOLEAN_OR_VALUE_REGEX = "(^(true)|(false)$)|(^(-)?[0-9]+(\\.[0-9]+)?$)"; 

	/********** ALL STATUS CODES USED **********/
	public static final int SUCCESS_STATUS_CODE = 200;
	public static final int AUTHORIZATION_FAILED_STATUS_CODE = 401;
	public static final int REQUEST_FAILED_CODE = 409;
	public static final int INTERNAL_SERVER_ERROR_CODE = 500;
	public static final int INVALID_POST_REQUEST_DATA_ERROR_CODE = 422;
	public static final int NOT_FOUND_ERROR_CODE = 404;

	/********** ALL KEYS USED **********/
	public static final String INTERNAL_SERVER_ERROR_KEY = "internalServerError";
	public static final String DATA_MESSAGE_KEY = "message";
	public static final String DATA_ERRORS_KEY = "errors";
	public static final String SECRET_KEY = "@#k)(u+-l!/i^%z+*a~!K)(U#$L$!I$+Z=#A";
	public static final String DATA_EXECUTION_ID="executionId";

	/********** MISCELLANEOUS CONSTANTS USED **********/
	public static final int MAX_LENGTH_STRING = 255;
	public static final int MAX_LENGTH_NAME_STRING = 100;
	public static final int MIN_LENGTH_STRING = 0;
	public static final int MAX_LENGTH_EXPRESSION = 10000;
	public static final int MIN_LENGTH_VARIABLE_EXPRESSION = 0;
	public static final int MIN_LENGTH_EXPRESSION = 0;
	public static final String RULES_PATH = "rules/";
	public static final String PRODUCTION_ENVIRONMENT = "production";
	public static final String TEST_ENVIRONMENT = "test";
	public static final String CONTAINER_ID = "Product_";

	/********** ALL MESSAGES USED **********/
	public static final String PRODUCT_CATEGORY_NAME_TOO_BIG = "Product Category Name Too Big";
	public static final String INVALID_OUTPUT_TYPE = "Invalid Output Type";
	public static final String INVALID_PRODUCT_NAME_MESSAGE = "Invalid Product Name";
	public static final String PRODUCT_NAME_TAKEN_MESSAGE = "Product Name Already Taken";
	public static final String PRODUCT_CATEGORY_NAME_TAKEN_MESSAGE = "Product Category Name Already Taken";
	public static final String INVALID_PRODUCT_CATEGORY_NAME_MESSAGE = "Invalid Product Category Name";
	public static final String INVALID_PRODUCT_CATEGORY_MESSAGE = "Invalid Product Category";
	public static final String INVALID_PRODUCT_ID_MESSAGE = "Invalid Product Id";
	public static final String INVALID_TEMPLATE_PRODUCT_ID_MESSAGE = "Invalid Template Product Id";
	public static final String PRODUCT_CATEGORY_MISMATCH_MESSAGE = "Template Product Category Doesn't Match With Selected Product Category";
	public static final String INVALID_PRODUCT_CATEGORY_ID_MESSAGE = "Invalid Product Category Id";
	public static final String PRODUCT_NAME_TOO_BIG_MESSAGE = "Product Name can be of max length 100";
	public static final String PRODUCT_CURRENTLY_DEPLOYED_MESSAGE = "Product Already Deployed";
	public static final String PRODUCT_ALREADY_EDITABLE_MESSAGE = "Product is Already In Edit Mode";
	public static final String PRODUCT_ALREADY_DEPLOYED_MESSAGE = "Product is Already Deployed once. Cannot make anymore changes to this Product";
	public static final String PRODUCT_NOT_PUBLISHED_MESSAGE = "Product Not Published. Publish Product Before Deploying";
	public static final String INVALID_PRODUCT_ID_OR_ALREADY_DEPLOYED_MESSAGE = "Product Id Invalid Or Product Already Deployed";
	public static final String PRODUCT_NOT_EDITABLE_MESSAGE = "Product Not Editable";
	public static final String PRODUCT_DEPLOYED_SUCCESS_MESSAGE = "Product Deployed Successfully";
	public static final String PRODUCT_PUBLISHED_SUCCESS_MESSAGE = "Product Published Successfully";
	public static final String PRODUCT_EDITABLE_SUCCESS_MESSAGE = "Product Can Now Be Edited";
	public static final String PRODUCT_CANNOT_BE_CLONED_MESSAGE = "Product Cannot be Cloned";
	public static final String PRODUCT_EXPRESSION_MISSING_MESSAGE = "Product Expression Missing";

	public static final String INVALID_PROCESS_INSTANCE_ID_MESSAGE = "Invalid Process Instance Id";

	public static final String INVALID_GROUP_ID_MESSAGE = "Invalid Group Id";
	public static final String INVALID_GROUP_NAME_MESSAGE = "Invalid Group Name";
	public static final String GROUP_NAME_TAKEN_MESSAGE = "Group Name Already Taken";
	public static final String INVALID_PRODUCT_GROUP_ID_MESSAGE = "Invalid Product Id or Group Id";
	public static final String GROUP_NAME_TOO_BIG_MESSAGE = "Group Name can be of max length 100";
	public static final String GROUP_MISSING_MESSAGE = "Groups Not Created";

	public static final String INVALID_VARIABLE_ID_MESSAGE = "Invalid Variable Id";
	public static final String INVALID_VARIABLE_NAME_MESSAGE = "Invalid Variable Name";
	public static final String VARIABLE_NAME_TAKEN_MESSAGE = "Variable Name Already Taken";
	public static final String INVALID_PRODUCT_VARIABLE_ID_MESSAGE = "Invalid Product Id or Variable Id";
	public static final String INVALID_VARIABLE_TYPE = "Invalid Variable Type";
	public static final String INVALID_VARIABLE_CATEGORY = "Invalid Variable Category";
	public static final String VARIABLE_SOURCE_TOO_BIG_MESSAGE = "Variable Source can be of max length 255";
	public static final String VARIABLE_DESCRIPTION_TOO_BIG_MESSAGE = "Variable Description can be of max length 255";
	public static final String VARIABLE_EXPRESSION_TOO_BIG_MESSAGE = "Variable Expression can be of max length 10000";
	public static final String VARIABLE_NAME_TOO_BIG_MESSAGE = "Variable Name can be of max length 100";
	public static final String VARIABLE_EXPRESSION_REQUIRED_MESSAGE = "Expression is required for derived variables";
	public static final String VARIABLE_DELETED_SUCCESS_MESSAGE = "Variable Deleted Successfully";
	public static final String VARIABLES_MISSING_MESSAGE = "Variables Not Created";

	public static final String INVALID_RULE_ID_MESSAGE = "Invalid Rule Id";
	public static final String CANNOT_DELETE_RULE_MESSAGE = "Cannot Delete This Rule. One Rule Must For This Variable";
	public static final String RULE_DELETED_SUCCESS_MESSAGE = "Rule Successfully Deleted";
	public static final String VARIABLE_ALREADY_USED = "Variable Already Used Once For Creating Rules";

	public static final String INVALID_EXPRESSION_MESSAGE = "Invalid Expression String";
	public static final String INVALID_EXPRESSION_ID_MESSAGE = "Invalid Expression Id";

	public static final String REGISTRATION_SUCCESSFUL_MESSAGE = "Registration Successful";
	public static final String INVALID_USERNAME_MESSAGE = "Invalid User Name";
	public static final String INVALID_PASSWORD_MESSAGE = "Invalid Password";
	public static final String INVALID_TOKEN_MESSAGE = "Invalid Access Token";
	public static final String USERNAME_TAKEN_MESSAGE = "User Name Already Taken";
	public static final String LOGOUT_SUCCESS_MESSAGE = "Logout Successful";

	public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error Occured";
	public static final String FAILURE_MESSAGE = "FAILED";
	public static final String SUCCESS_MESSAGE = "SUCCESS";

	/********** ALL VARIABLE KEYS USED **********/
	public static final String USER_ID = "userId";
	public static final String INVALID_USER_ID = "userId.invalid";
	public static final String PRODUCT_ID = "productId";
	public static final String INVALID_PRODUCT_ID = "productId.invalid";
	public static final String TEMPLATE_PRODUCT_ID = "templateProductId";
	public static final String INVALID_TEMPLATE_PRODUCT_ID = "templateProductId.invalid";
	public static final String PRODUCT_CATEGORY_ID = "productCategoryId";
	public static final String INVALID_PRODUCT_CATEGORY_ID = "productCategoryId.invalid";
	public static final String GROUP_ID = "groupId";
	public static final String INVALID_GROUP_ID = "groupId.invalid";
	public static final String VARIABLE_ID = "variableId";
	public static final String INVALID_VARIABLE_ID = "variableId.invalid";
	public static final String RULE_ID = "ruleId";
	public static final String INVALID_RULE_ID = "ruleId.invalid";
	public static final String EXPRESSION_ID = "expressionId";
	public static final String INVALID_EXPRESSION_ID = "expressionId.invalid";

	public static final String NUMERICAL = "Numerical";
	public static final String STRING = "String";
	public static final String BOOLEAN = "Boolean";
	public static final String OUTPUT = "output";
	public static final String GROUP_FILTER = "groupFilter";
	public static final String PRODUCT_FILTER = "productFilter";
	public static final String PRIMITIVE = "Primitive";
	public static final String DERIVED = "Derived";
	public static final String NAME = "name";
	public static final String TYPE = "type";
	public static final String VALUE = "value";
	public static final String ERROR = "error";
	public static final String MISSING = "Missing";
	public static final String INVALID_VALUE = "Invalid Value";

	public static final String RULES = "rules";
	public static final String GROUPS = "groups";
	public static final String VARIABLES = "variables";
	public static final String EXPRESSIONS = "expressions";

	public static final String VALUE_1 = "value1";
	public static final String INVALID_VALUE_1 = "value1.invalid";
	public static final String VALUE_2 = "value2";
	public static final String INVALID_VALUE_2 = "value2.invalid";
	public static final String FN_1 = "fn1";
	public static final String INVALID_FN_1 = "fn1.invalid";
	public static final String FN_2 = "fn2";
	public static final String INVALID_FN_2 = "fn2.invalid";
	public static final String USER_NAME = "userName";
	public static final String INVALID_USER_NAME = "userName.invalid";
	public static final String VARIABLE_NAME = "variableName";
	public static final String INVALID_VARIABLE_NAME = "variableName.invalid";
	public static final String GROUP_NAME = "groupName";
	public static final String INVALID_GROUP_NAME = "groupName.invalid";
	public static final String PRODUCT_NAME = "productName";
	public static final String INVALID_PRODUCT_NAME = "productName.invalid";
	public static final String INPUT_DATA = "inputData";
	public static final String INVALID_INPUT_DATA = "inputData.invalid";
	public static final String PRODUCT_CATEGORY_NAME = "productCategoryName";
	public static final String INVALID_PRODUCT_CATEGORY_NAME = "productCategoryName.invalid";
	public static final String EXPRESSION_STRING = "expressionString";
	public static final String INVALID_EXPRESSION_STRING = "expressionString.invalid";
	public static final String IDENTIFIER = "identifier";
	public static final String INVALID_IDENTIFIER = "identifier.invalid";
	public static final String PRODUCT_IDS = "productIds";
	public static final String INVALID_PRODUCT_IDS = "productIds.invalid";
	public static final String INVALID_STATUS_MESSAGE = "Invalid Status Value";
	
	public static final String OUTPUT_DATA="data";
	public static final String NO_TEST_CASES="No Test Cases Provided";
	public static final String MISSING_FIELD_VALUE="Missing value for field '";

}
