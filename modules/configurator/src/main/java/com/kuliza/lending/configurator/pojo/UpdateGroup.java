package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UpdateGroup {

	@NotNull(message = "groupName is a required key")
	@Pattern(regexp = Constants.NAME_REGEX, message = Constants.INVALID_GROUP_NAME_MESSAGE)
	@Size(max = Constants.MAX_LENGTH_NAME_STRING, message = Constants.GROUP_NAME_TOO_BIG_MESSAGE)
	private String groupName;

	private String userId;
	private String productId;
	private String groupId;

	public UpdateGroup() {
		this.groupName = "";
	}

	public UpdateGroup(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	// @Override
	// public String toString() {
	// StringBuilder updateVariable = new StringBuilder();
	// updateVariable.append("{ ");
	// updateVariable.append("groupName : " + groupName);
	// updateVariable.append(" }");
	// return updateVariable.toString();
	//
	// }

}
