package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.configurator.serializers.ExpressionSerializer;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "ce_expression")
@JsonSerialize(converter = ExpressionSerializer.class)
@Where(clause = "is_deleted=0")
public class Expression extends BaseModel {

	@Column(length = 10000, nullable = false)
	private String expressionString;
	@Column(columnDefinition = "tinyint(1) default 1", nullable = false)
	private boolean isGroup;
	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private boolean isProduct;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "productId")
	private Product product;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "groupId")
	private Group group;

	public Expression() {
		super();
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public Expression(long id, String expressionString, boolean isGroup, boolean isProduct, Product product,
			Group group) {
		super();
		this.setId(id);
		this.expressionString = expressionString;
		this.isGroup = isGroup;
		this.isProduct = isProduct;
		this.product = product;
		this.group = group;
	}

	public Expression(String expressionString, boolean isGroup, boolean isProduct, Product product, Group group) {
		this.expressionString = expressionString;
		this.isGroup = isGroup;
		this.isProduct = isProduct;
		this.product = product;
		this.group = group;
		this.setCreated(new Timestamp(new Date().getTime()));
		this.setModified(new Timestamp(new Date().getTime()));
		this.setIsDeleted(false);
	}

	public String getExpressionString() {
		return expressionString;
	}

	public void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	@JsonIgnore
	public boolean isGroup() {
		return isGroup;
	}

	public void setGroup(boolean isGroup) {
		this.isGroup = isGroup;
	}

	@JsonIgnore
	public boolean isProduct() {
		return isProduct;
	}

	public void setProduct(boolean isProduct) {
		this.isProduct = isProduct;
	}

	@JsonIgnore
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@JsonIgnore
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
