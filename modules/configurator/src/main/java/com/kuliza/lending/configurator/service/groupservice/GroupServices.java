package com.kuliza.lending.configurator.service.groupservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.Product;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.service.genericservice.GenericServicesCE;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class GroupServices extends GenericServicesCE {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	public GenericAPIResponse getGroupsList(String productId) throws Exception {
		List<Group> allUserGroups = groupDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.GROUP_FILTER,
				SimpleBeanPropertyFilter.serializeAllExcept(Constants.RULES));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allUserGroups);
	}

	public GenericAPIResponse getGroupsData(String productId) throws Exception {
		List<Group> allUserGroups = groupDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.GROUP_FILTER,
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allUserGroups);
	}

	public GenericAPIResponse getGroupData(String groupId) throws Exception {
		Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(groupId), false);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.GROUP_FILTER,
				SimpleBeanPropertyFilter.serializeAll());
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, group);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createNewGroup(SubmitNewGroup input) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(input.getProductId()), false);
		String groupName = input.getGroupName();
		Group newGroup = new Group(groupName, product);
		groupDao.save(newGroup);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.GROUP_FILTER,
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", Constants.RULES));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, newGroup);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse updateGroup(UpdateGroup input) throws Exception {
		Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(input.getGroupId()), false);
		group.setName(input.getGroupName());
		group.setModified(new Timestamp(new Date().getTime()));
		groupDao.save(group);
		FilterProvider filters = new SimpleFilterProvider().addFilter(Constants.GROUP_FILTER,
				SimpleBeanPropertyFilter.serializeAllExcept("expressions", Constants.RULES));
		objectMapper.setFilterProvider(filters);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, group);
	}

}
