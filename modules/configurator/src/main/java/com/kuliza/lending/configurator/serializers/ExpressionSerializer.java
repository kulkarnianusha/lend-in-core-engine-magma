package com.kuliza.lending.configurator.serializers;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.kuliza.lending.configurator.models.Expression;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.VariableDao;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ExpressionSerializer extends StdConverter<Expression, Map<String, Object>> {

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private GroupDao groupDao;

	@Override
	public Map<String, Object> convert(Expression expression) {
		try {
			Map<String, Object> expressionMap = new HashMap<>();
			if (expression.isGroup()) {
				expressionMap.put("id", expression.getId());
				expressionMap.put("expressionString", HelperFunctions
						.makeReadableExpressionString(expression.getExpressionString(), variableDao, groupDao, true));
			} else {
				expressionMap.put("id", expression.getId());
				expressionMap.put("expressionString", HelperFunctions
						.makeReadableExpressionString(expression.getExpressionString(), variableDao, groupDao, false));
			}
			return expressionMap;
		} catch (Exception e) {
			// Here we are setting expression string to null but sending
			// expressionId
			// so that user can add the new expression for the same expressionId
			Map<String, Object> expressionMap = new HashMap<>();
			expressionMap.put("id", expression.getId());
			expressionMap.put("expressionString", "");
			expression.setExpressionString("");
			return expressionMap;
		}
	}
}