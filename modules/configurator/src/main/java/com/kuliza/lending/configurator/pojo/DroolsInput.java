package com.kuliza.lending.configurator.pojo;

import java.util.HashMap;
import java.util.Map;

public class DroolsInput {

	private Map<String, Object> inputValues;
	private Map<String, Object> outputScores;

	public DroolsInput() {
		super();
	}

	public DroolsInput(Map<String, Object> inputValues) {
		this.inputValues = inputValues;
		this.outputScores = new HashMap<>();
	}

	public Map<String, Object> getInputValues() {
		return inputValues;
	}

	public void setInputValues(Map<String, Object> inputValues) {
		this.inputValues = inputValues;
	}

	public Map<String, Object> getOutputScores() {
		return outputScores;
	}

	public void setOutputScores(Map<String, Object> outputScores) {
		this.outputScores = outputScores;
	}

	public void setSingleOutputScore(String key, Object output) {
		outputScores.put(key, output);
	}

	public Object getSingleOutputScore(String key) {
		return outputScores.get(key);
	}
}
