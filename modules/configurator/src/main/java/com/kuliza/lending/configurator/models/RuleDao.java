package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface RuleDao extends CrudRepository<Rule, Long> {
	public Rule findById(long id);

	public Rule findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Rule> findByGroupId(long groupId);

	public List<Rule> findByGroupIdAndIsActive(long groupId, boolean isActive);

	public List<Rule> findByGroupIdAndIsDeleted(long groupId, boolean isDeleted);

	public Rule findByIdAndGroupIdAndIsDeleted(long id, long groupId, boolean isDeleted);

	public List<Rule> findByGroupIdAndIsDeletedAndIsActive(long groupId, boolean isDeleted, boolean isActive);

	public List<Rule> findByVariableIdAndIsDeleted(long variableId, boolean isDeleted);
}
