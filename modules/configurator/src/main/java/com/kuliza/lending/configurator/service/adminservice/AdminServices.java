package com.kuliza.lending.configurator.service.adminservice;

import com.kuliza.lending.configurator.models.ProductCategory;
import com.kuliza.lending.configurator.models.ProductCategoryDao;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProductCategory;
import com.kuliza.lending.configurator.service.genericservice.GenericServicesCE;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminServices extends GenericServicesCE {

	@Autowired
	private ProductCategoryDao productCategoryDao;

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createProductCategory(SubmitNewProductCategory input) throws Exception {
		ProductCategory productCategory = new ProductCategory(input.getProductCategoryName());
		productCategoryDao.save(productCategory);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, productCategory);
	}

}
