package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.GetRequiredVariables;
import com.kuliza.lending.configurator.pojo.ProductionFireRulesInput;
import com.kuliza.lending.configurator.pojo.TestFireRulesInput;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.*;

@Component
public class FireRulesDataValidator extends BasicDataValidator {

	@Autowired
	private ProductDao productDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDeploymentDao productDeploymentDao;

	public void validateAllRequiredVariables(GetRequiredVariables input, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		for (Long productId : input.getProductIds()) {
			if (productDao.findByIdAndUserIdAndIsDeleted(productId, Long.parseLong(input.getUserId()), false) == null) {
				errors.rejectValue(Constants.PRODUCT_IDS, Constants.INVALID_PRODUCT_IDS,
						"Invalid Product Id : " + productId);
			}
		}
	}

	public Map<String, String> checkVariable(Map<String, Object> input, Map<String, String> requiredVariable,
			String requiredVariableName, Long productId) {
		Map<String, String> v = new HashMap<>();
		if (!input.containsKey(requiredVariableName)) {
			v.put(Constants.NAME, requiredVariable.get(Constants.NAME));
			v.put(Constants.TYPE, requiredVariable.get(Constants.TYPE));
			v.put(Constants.VALUE, "");
			v.put(Constants.ERROR, Constants.MISSING);
		} else {
			String value = input.get(requiredVariableName).toString();
			Variable variable = variableDao.findByNameAndProductIdAndIsDeleted(requiredVariable.get(Constants.NAME),
					productId, false);
			if ((variable.getType().equals(Constants.BOOLEAN) && !value.matches(Constants.IS_BOOLEAN_REGEX))
					|| (variable.getType().equals(Constants.NUMERICAL) && !value.matches(Constants.VALUE_REGEX))) {
				v.put(Constants.NAME, requiredVariable.get(Constants.NAME));
				v.put(Constants.TYPE, requiredVariable.get(Constants.TYPE));
				v.put(Constants.VALUE, value);
				v.put(Constants.ERROR, Constants.INVALID_VALUE);
			}
		}
		return v;
	}

	public void validateTestFireRules(TestFireRulesInput obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		List<Long> productIds = obj.getProductIds();
		for (Long id : productIds) {
			Product product = productDao.findByIdAndUserIdAndIsDeleted(id, Long.parseLong(obj.getUserId()), false);
			if (product != null && product.getStatus() < 1) {
				errors.rejectValue(Constants.PRODUCT_IDS, Constants.INVALID_PRODUCT_IDS,
						"Product : " + product.getName() + " Not Published");
			} else if (product == null) {
				errors.rejectValue(Constants.PRODUCT_IDS, Constants.INVALID_PRODUCT_IDS, "Invalid Product Id : " + id);
			}
		}
	}

	public GenericAPIResponse checkRequiredVariables(Long productId, Set<Map<String, String>> allRequiredVariables,
			Map<String, Object> input, Boolean flag) throws Exception {
		GenericAPIResponse response = null;
		Set<Map<String, String>> missingOrInvalidVariables = new HashSet<>();
		for (Map<String, String> requiredVariable : allRequiredVariables) {
			String nameOfVariableInInputData;
			if (flag) {
				nameOfVariableInInputData = requiredVariable.get(Constants.NAME) + "-"
						+ requiredVariable.get(Constants.TYPE);
			} else {
				nameOfVariableInInputData = requiredVariable.get(Constants.NAME);
			}
			Map<String, String> v = checkVariable(input, requiredVariable, nameOfVariableInInputData, productId);
			if (!v.isEmpty()) {
				missingOrInvalidVariables.add(v);
			}
		}
		if (!missingOrInvalidVariables.isEmpty()) {
			Map<String, Object> data = new HashMap<>();
			data.put("errors", missingOrInvalidVariables);
			data.put("input", input);
			response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, Constants.FAILURE_MESSAGE,
					data);
		}
		return response;
	}

	public GenericAPIResponse validateTestEachProductFireRules(Long productId, Long userId, Map<String, Object> input,
			Set<Map<String, String>> allRequiredVariables) throws Exception {
		return checkRequiredVariables(productId, allRequiredVariables, input, true);
	}

	public void validateProductionFireRules(ProductionFireRulesInput obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (productDeploymentDao.findByIdentifierAndUserIdAndStatusAndIsDeleted(obj.getIdentifier(),
				Long.parseLong(obj.getUserId()), true, false) == null) {
			errors.rejectValue(Constants.IDENTIFIER, Constants.INVALID_IDENTIFIER,
					"Invalid Identifier or Product Not Deployed");
		}
	}

	public GenericAPIResponse validateProductionProductFireRules(ProductionFireRulesInput obj,
			Set<Variable> primitiveVariables, Set<Variable> derivedVariable) throws Exception {
		Product product = productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(obj.getIdentifier(), true, false)
				.getProduct();
		Set<Map<String, String>> allRequiredVariables = findAllVariablesRequiredForProduct(product.getId(),
				primitiveVariables, derivedVariable);
		return checkRequiredVariables(product.getId(), allRequiredVariables, obj.getInputData(), false);
	}

	public GenericAPIResponse validateProductionGroupFireRules(ProductionFireRulesInput obj,
			Set<Variable> primitiveVariables, Set<Variable> derivedVariable) throws Exception {
		GenericAPIResponse response = null;
		Product product = productDeploymentDao.findByIdentifierAndStatusAndIsDeleted(obj.getIdentifier(), true, false)
				.getProduct();
		if (!validateGroupId(obj.getGroupId(), Long.toString(product.getId()))) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_GROUP_ID_MESSAGE);
		} else {
			Group group = groupDao.findByIdAndIsDeleted(Long.parseLong(obj.getGroupId()), false);
			Set<Variable> allProcessedVariables = new HashSet<>();
			Set<Map<String, String>> allRequiredVariables = findAllVariablesRequiredForGroup(group.getId(),
					allProcessedVariables, primitiveVariables, derivedVariable);
			response = checkRequiredVariables(product.getId(), allRequiredVariables, obj.getInputData(), false);
		}
		return response;
	}

}
