package com.kuliza.lending.configurator.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.kuliza.lending.configurator.utils.Constants;

public class RegisterUser {

	@NotNull(message = "emailId is a required key")
	@NotEmpty(message = "emailId cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "emailId can be of max length 255")
	String emailId;

	@NotNull(message = "password is a required key")
	@NotEmpty(message = "Password cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "Password can be of max length 255")
	String password;

	@NotNull(message = "role is a required key")
	@NotEmpty(message = "role cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "role can be of max length 255")
	String role;

	public RegisterUser() {
		super();
	}

	public RegisterUser(String emailId, String password, String role) {
		super();
		this.emailId = emailId;
		this.password = password;
		this.role = role;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
