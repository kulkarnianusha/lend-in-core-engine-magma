package com.kuliza.lending.common.exception;

public class ProcessInstanceDoesNotExistException extends Exception {

	public ProcessInstanceDoesNotExistException(String processInstanceId) {
		super("No process instance exist with given process instance id : " + processInstanceId);
	}

}
