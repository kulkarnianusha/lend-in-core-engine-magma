package com.kuliza.lending.common.utils;

public class Enums {

	public enum AddressType {
		CURRENT, PERMANENT
	}

	public enum EmploymentStatus {
		SALARIED, SELF_EMPLOYED
	}

	public enum ApplicationStage {
		NEW, APPROVED, EXPIRED, CANCELLED, DISBURSED, CLOSED, REJECTED, MIGRATION_FAILED, MIGRATION_SUCCESS
	}

	public enum NotificationMessageStructure {
		string, json
	}

	public enum NotificationsCategory {
		billing, support, maintenance
	}

	public enum CustomerDashboardCustomerType {
		PROSPECT, EXISTING
	}

	public enum APPLICATION_STAGE {

		NEW, APPROVED, EXPIRED, CANCELLED, DISBURSED, CLOSED, REJECTED, MIGRATION_FAILED, MIGRATION_SUCCESS

	}

}
