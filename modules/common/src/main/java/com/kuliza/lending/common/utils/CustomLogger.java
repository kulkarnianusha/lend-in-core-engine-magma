package com.kuliza.lending.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.pojo.HTTPResponse;

public class CustomLogger {

	private static final Logger logger = LoggerFactory.getLogger(CustomLogger.class);

	public static void putDataInMDC(String userId, String processInstanceId, LogType type, JobType job, String appId,
			String losId, String statusCode) {
		MDC.put("userId", userId != null ? userId : "");
		MDC.put("processInstanceId", processInstanceId != null ? processInstanceId : "");
		MDC.put("type", type != null ? type.toString() : "");
		MDC.put("job", job != null ? job.toString() : "");
		MDC.put("appId", appId != null ? appId : "");
		MDC.put("losId", losId != null ? losId : "");
		MDC.put("statusCode", statusCode != null ? statusCode : "");
	}

	public static void log(String userId, String processInstanceId, LogType type, JobType job, String appId,
			String losId, Object msg) throws JsonProcessingException {
		if (logger.isInfoEnabled()) {
			String status = "";
			if (msg instanceof ApiResponse) {
				ApiResponse outputResponse = (ApiResponse) msg;
				status = Integer.toString(outputResponse.getStatus());
			} else if (msg instanceof HTTPResponse) {
				HTTPResponse responseFromOutboundApiCall = (HTTPResponse) msg;
				status = Integer.toString(responseFromOutboundApiCall.getStatusCode());
			}
			putDataInMDC(userId, processInstanceId, type, job, appId, losId, status);
			logger.info(StaticContextAccessor.getBean(ObjectMapper.class).writeValueAsString(msg));
		}
	}

	public static void logException(String userId, String processInstanceId, LogType type, JobType job, String appId,
			String losId, Exception e, Object msg) {
		if (logger.isErrorEnabled()) {
			try {
				putDataInMDC(userId, processInstanceId, type, job, appId, losId, null);
				logger.error(StaticContextAccessor.getBean(ObjectMapper.class).writeValueAsString(msg), e);
			} catch (Exception jsonParseException) {
				logger.error(msg.toString(), jsonParseException);
			}
		}
	}

}
