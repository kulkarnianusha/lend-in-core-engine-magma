package com.kuliza.lending.common.connection;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;

import com.kuliza.lending.common.pojo.HTTPResponse;

public class HTTPConnection {

	private final static String USER_AGENT = "Mozilla/5.0";

	/**
	 * 
	 * Sends HTTP Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param data
	 *            RAW Data for the request body, eligible only for POST/PUT.
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @param requestMethod
	 *            GET/POST/PUT/DELETE
	 * @return
	 * @throws IOException
	 */

	public static HTTPResponse send(String protocol, String url, Integer port, String suburl, String data,
			String requestMethod, String token) throws IOException {

		URL obj = null;
		if (port != 1) {
			obj = new URL(protocol, url, port, suburl);
		} else {
			obj = new URL(protocol, url, suburl);
		}
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// add request header
		con.setRequestMethod(requestMethod);
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		if (token != null) {
			con.setRequestProperty("Authorization", "Bearer " + token);
		}
		// Send post request
		con.setDoOutput(true);
		if (data != null) {
			try {
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), "UTF-8"));
				bw.write(data);
				bw.flush();
				bw.close();
			} catch (ConnectException e) {
				return new HTTPResponse(HttpStatus.NOT_FOUND.value(), "Connection Refused.");
			}
		}
		int code = con.getResponseCode();
		byte[] bytes;
		if (code != HttpStatus.OK.value() && code != HttpStatus.ACCEPTED.value()) {
			bytes = IOUtils.toByteArray(con.getErrorStream());
			return new HTTPResponse(code, new String(bytes, "UTF-8"));
		} else {
			bytes = IOUtils.toByteArray(con.getInputStream());
			return new HTTPResponse(code, new String(bytes, "UTF-8"));
		}
	}

	/**
	 * Send HTTP Post Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param data
	 *            RAW Data for the request body, eligible only for POST/PUT.
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendPOST(String protocol, String url, Integer port, String suburl, String data)
			throws IOException {

		return send(protocol, url, port, suburl, data, "POST", null);
	}

	/**
	 * Send HTTP Post Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param data
	 *            RAW Data for the request body, eligible only for POST/PUT.
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @param token
	 *            Auth Token
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendPOST(String protocol, String url, Integer port, String suburl, String data,
			String token) throws IOException {

		return send(protocol, url, port, suburl, data, "POST", token);
	}

	/**
	 * Send HTTP Post Request.`
	 * 
	 * @param url
	 *            Base URL
	 * @param data
	 *            RAW Data for the request body, eligible only for POST/PUT.
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendPOST(String protocol, String url, String data) throws IOException {

		return sendPOST(protocol, url, 1, "", data);
	}

	/**
	 * 
	 * Sends HTTP DELETE Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendDELETE(String protocol, String url, Integer port, String suburl) throws IOException {

		return send(protocol, url, port, suburl, null, "DELETE", null);
	}

	/**
	 * 
	 * Sends HTTP GET Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendGET(String protocol, String url, Integer port, String subURL) throws IOException {

		return send(protocol, url, port, subURL, null, "GET", null);
	}

	public static HTTPResponse sendGET(String protocol, String url, Integer port, String subURL, String data)
			throws IOException {

		return send(protocol, url, port, subURL, data, "GET", null);
	}

	public static HTTPResponse sendGET(String protocol, String url, Integer port, String subURL, String data,
			String token) throws IOException {

		return send(protocol, url, port, subURL, data, "GET", token);
	}

	/**
	 * 
	 * Sends HTTP PUT Request.
	 * 
	 * @param url
	 *            Base URL
	 * @param port
	 *            Port, For Default : 1
	 * @param suburl
	 *            Sub URL to be hit as the POST.
	 * @return
	 * @throws IOException
	 */
	public static HTTPResponse sendPUT(String protocol, String url, Integer port, String subURL, String data)
			throws IOException {
		return send(protocol, url, port, subURL, data, "PUT", null);

	}
}