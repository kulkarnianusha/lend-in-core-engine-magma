package com.kuliza.lending.collections.repository;

import com.kuliza.lending.collections.model.DecisionTableModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


//This is the repository for decision table, to interact with the model based on the functions defined in it.
@Repository
public interface DecisionTableRepository extends JpaRepository<DecisionTableModel, Long> {



    //This is the function which returns the first entry for the provided nameKey and isDeleted ordered by version in descending order
    DecisionTableModel findFirstByNameKeyAndIsDeletedOrderByVersionDesc(String nameKey, boolean isDeleted);

    //This is the function which returns the first entry for the provided nameKey ordered by version in descending order
    DecisionTableModel findFirstByNameKeyOrderByVersionDesc(String nameKey);

    //This is the function which returns the decision table for provided nameKey and version
    DecisionTableModel findByNameKeyAndVersionAndIsDeleted(String nameKey, int version, boolean isDeleted);



}
