package com.kuliza.lending.backoffice.pojo.dashboard.config;

import com.kuliza.lending.backoffice.models.Outcomes;

public class OutcomesConfig {

	private String label;
	private String value;
	private boolean comment_required;

	public OutcomesConfig() {
		super();
	}

	public OutcomesConfig(String label, String value, boolean comment_required) {
		super();
		this.label = label;
		this.value = value;
		this.comment_required = comment_required;
	}

	public OutcomesConfig(Outcomes outcome) {
		super();
		this.label = outcome.getLabel();
		this.value = outcome.getOutcomeKey();
		this.comment_required = outcome.isCommentRequired();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isComment_required() {
		return comment_required;
	}

	public void setComment_required(boolean comment_required) {
		this.comment_required = comment_required;
	}

}
