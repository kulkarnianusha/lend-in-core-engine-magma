package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.List;
import java.util.Set;

public class TabConfig {

	private String label;
	private String key;
	private Set<String> variables;
	private List<CardConfig> cards;

	public TabConfig() {
		super();
	}

	public TabConfig(String label, String key, Set<String> variables, List<CardConfig> cards) {
		super();
		this.label = label;
		this.key = key;
		this.variables = variables;
		this.cards = cards;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Set<String> getVariables() {
		return variables;
	}

	public void setVariables(Set<String> variables) {
		this.variables = variables;
	}

	public List<CardConfig> getCards() {
		return cards;
	}

	public void setCards(List<CardConfig> cards) {
		this.cards = cards;
	}

}
