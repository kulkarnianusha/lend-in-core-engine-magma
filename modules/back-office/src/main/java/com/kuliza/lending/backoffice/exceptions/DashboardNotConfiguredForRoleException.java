package com.kuliza.lending.backoffice.exceptions;

public class DashboardNotConfiguredForRoleException extends RuntimeException {

	public DashboardNotConfiguredForRoleException() {
		super();
	}

	public DashboardNotConfiguredForRoleException(String message) {
		super(message);
	}
}
