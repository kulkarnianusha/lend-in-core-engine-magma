package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_roles")
public class Roles extends BaseModel {

	@Column(nullable = false, unique = true)
	private String roleName;

	@Column(columnDefinition = "tinyint(1) default 1")
	private boolean enableComments;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "role", orphanRemoval = true)
	@OrderBy("tabOrder ASC")
	private Set<Tabs> tabs;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "role", orphanRemoval = true)
	@OrderBy("outcomesOrder ASC")
	private Set<Outcomes> outcomes;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "role", orphanRemoval = true)
	@OrderBy("bucketOrder ASC")
	private Set<Buckets> buckets;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "role", orphanRemoval = true)
	private Set<Variables> variables;

	public Roles() {
		super();
		this.setIsDeleted(false);
	}

	public Roles(String roleName, boolean enableComments) {
		super();
		this.roleName = roleName;
		this.enableComments = enableComments;
		this.setIsDeleted(false);
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<Tabs> getTabs() {
		return tabs;
	}

	public Set<Outcomes> getOutcomes() {
		return outcomes;
	}

	public Set<Buckets> getBuckets() {
		return buckets;
	}

	public Set<Variables> getVariables() {
		return variables;
	}

	public boolean isEnableComments() {
		return enableComments;
	}

	public void setEnableComments(boolean enableComments) {
		this.enableComments = enableComments;
	}

}
