package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_tabs", uniqueConstraints = { @UniqueConstraint(columnNames = { "tabKey", "role_id" }) })
public class Tabs extends BaseModel {

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private int tabOrder;

	@Column(nullable = false)
	private String tabKey;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "role_id")
	private Roles role;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "tab", orphanRemoval = true)
	@OrderBy("cardsOrder ASC")
	private Set<Cards> cards;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "tab", orphanRemoval = true)
	@OrderBy("tabVariableOrder ASC")
	private Set<TabsVariablesMapping> tabVariables;

	public Tabs() {
		super();
		this.setIsDeleted(false);
	}

	public Tabs(String name, int tabOrder, String tabKey, Roles role) {
		super();
		this.name = name;
		this.tabOrder = tabOrder;
		this.tabKey = tabKey;
		this.role = role;
		this.setIsDeleted(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTabOrder() {
		return tabOrder;
	}

	public void setTabOrder(int tabOrder) {
		this.tabOrder = tabOrder;
	}

	public Set<Cards> getCards() {
		return cards;
	}

	public void setCards(Set<Cards> cards) {
		this.cards = cards;
	}

	public String getTabKey() {
		return tabKey;
	}

	public void setTabKey(String tabKey) {
		this.tabKey = tabKey;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Set<TabsVariablesMapping> getTabVariables() {
		return tabVariables;
	}

}
