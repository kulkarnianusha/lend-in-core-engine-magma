package com.kuliza.lending.backoffice.pojo.dashboard;

import javax.validation.constraints.NotNull;

public class SubmitCommentRequest {

	@NotNull(message = "message cannot be null")
	private String message;

	public SubmitCommentRequest() {
		super();
	}

	public SubmitCommentRequest(
			@NotNull(message = "message cannot be null") String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
