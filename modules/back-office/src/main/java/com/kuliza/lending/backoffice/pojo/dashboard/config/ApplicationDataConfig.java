package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.kuliza.lending.backoffice.models.Outcomes;
import com.kuliza.lending.backoffice.models.Roles;
import com.kuliza.lending.backoffice.models.Tabs;

public class ApplicationDataConfig {

	private boolean enable_comments;

	private List<TabsConfig> tabs;

	private List<OutcomesConfig> outcomes;

	public ApplicationDataConfig() {
		super();
	}

	public ApplicationDataConfig(boolean enable_comments, List<TabsConfig> tabs, List<OutcomesConfig> outcomes) {
		super();
		this.enable_comments = enable_comments;
		this.tabs = tabs;
		this.outcomes = outcomes;
	}

	public ApplicationDataConfig(Roles role, Set<Tabs> tabs, Set<Outcomes> outcomes) {
		super();
		this.enable_comments = role.isEnableComments();
		this.tabs = new ArrayList<>();
		this.outcomes = new ArrayList<>();
		for (Tabs tab : tabs) {
			this.tabs.add(new TabsConfig(tab));
		}

		for (Outcomes outcome : outcomes) {
			this.outcomes.add(new OutcomesConfig(outcome));
		}
	}

	public List<TabsConfig> getTabs() {
		return tabs;
	}

	public void setTabs(List<TabsConfig> tabs) {
		this.tabs = tabs;
	}

	public List<OutcomesConfig> getOutcomes() {
		return outcomes;
	}

	public void setOutcomes(List<OutcomesConfig> outcomes) {
		this.outcomes = outcomes;
	}

	public boolean isEnable_comments() {
		return enable_comments;
	}

	public void setEnable_comments(boolean enable_comments) {
		this.enable_comments = enable_comments;
	}

}
