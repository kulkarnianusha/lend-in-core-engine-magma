package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.List;

public class DashboardConfig {

	private List<RoleConfig> roles;

	public DashboardConfig() {
		super();
	}

	public DashboardConfig(List<RoleConfig> roles) {
		super();
		this.roles = roles;
	}

	public List<RoleConfig> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleConfig> roles) {
		this.roles = roles;
	}

}
