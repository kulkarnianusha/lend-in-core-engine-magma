package com.kuliza.lending.backoffice.pojo.dashboard;

public class CommentFilter {

	private String id;
	private String user;

	public CommentFilter() {
		super();
	}

	public CommentFilter(String id, String user) {
		super();
		this.id = id;
		this.user = user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
