package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.List;

import com.kuliza.lending.backoffice.pojo.dashboard.config.OptionsConfig;

public class VariableOptionListConfig extends VariableConfig {

	private List<OptionsConfig> options;

	public List<OptionsConfig> getOptions() {
		return options;
	}

	public void setOptions(List<OptionsConfig> options) {
		this.options = options;
	}

	public VariableOptionListConfig(List<OptionsConfig> options) {
		super();
		this.options = options;
	}

	public VariableOptionListConfig() {
		super();
	}

	public VariableOptionListConfig(String label, String key, boolean editable, boolean writable, String meta,
			String type) {
		super(label, key, editable, writable, meta, type);
	}

}
