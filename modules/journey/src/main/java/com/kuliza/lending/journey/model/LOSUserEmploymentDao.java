package com.kuliza.lending.journey.model;

import org.springframework.data.repository.CrudRepository;

public interface LOSUserEmploymentDao extends CrudRepository<LOSUserEmploymentModel, Long> {

	public LOSUserEmploymentModel findById(long id);

	public LOSUserEmploymentModel findByIdAndIsDeleted(long id, boolean isDeleted);

	public LOSUserEmploymentModel findByLosUserModel(LOSUserModel user);
}
