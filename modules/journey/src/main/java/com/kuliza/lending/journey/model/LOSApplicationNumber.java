package com.kuliza.lending.journey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "los_application_no")
public class LOSApplicationNumber extends BaseModel {

	@Column(nullable = true)
	private String processInstanceId;

	@Column(nullable = true)
	private String loanApplicationNumber;

	public LOSApplicationNumber() {
		super();
		this.processInstanceId = null;
		this.loanApplicationNumber = null;
		this.setIsDeleted(false);
	}

	public LOSApplicationNumber(String processInstanceId) {
		super();
		this.processInstanceId = processInstanceId;
		this.setIsDeleted(false);
	}

	public LOSApplicationNumber(String processInstanceId, String loanApplicationNumber) {
		super();
		this.setIsDeleted(false);
		this.processInstanceId = processInstanceId;
		this.loanApplicationNumber = loanApplicationNumber;
	}

	public String getLoanApplicationNumber() {
		return loanApplicationNumber;
	}

	public void setLoanApplicationNumber(String loanApplicationNumber) {
		this.loanApplicationNumber = loanApplicationNumber;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

}