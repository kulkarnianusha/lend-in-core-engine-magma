package com.kuliza.lending.journey.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class OtpLoginInputform {

	@NotNull(message = "mobile cannot be null")
	@Size(min = 9, max = 11)
	@Pattern(regexp = "^(([1-9])[0-9]{8,9})$")
	String mobile;

	@NotNull(message = "otp cannot be null")
	@Size(min = 4, max = 6)
	@Pattern(regexp = "[0-9]+")
	String otp;

	public OtpLoginInputform() {

	}

	public OtpLoginInputform(
			@NotNull(message = "mobile cannot be null") @Size(min = 9, max = 11) @Pattern(regexp = "^((0([1-9]))[0-9]{8,9})$") String mobile,
			@NotNull(message = "otp cannot be null") @Size(min = 4, max = 4) @Pattern(regexp = "[0-9]+") String otp) {
		super();
		this.mobile = mobile;
		this.otp = otp;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
